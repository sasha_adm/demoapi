<?php
namespace SashaAdm\Demoapi;

use Illuminate\Support\ServiceProvider;



class DemoapiServiceProvider extends ServiceProvider
{

    public function boot(){

        $this->loadRoutesFrom(__DIR__.'/Http/routes.php');

        $this->loadViewsFrom(__DIR__ . '/Views', 'demoapi');

        $this->loadMigrationsFrom(__DIR__.'/migrations');

        $this->publishes([
            __DIR__ . '/migrations/' => base_path('/database/migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/img/' => base_path('/storage/framework/views'),
        ], 'img');


    }


    public function register(){


        $this->publishes([
            __DIR__.'/img' => resource_path('storage/framework/views'),
        ]);



    }

}