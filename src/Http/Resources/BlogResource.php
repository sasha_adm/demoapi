<?php

namespace SashaAdm\Demoapi\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'user_name' => optional($this->user)->name,
            'body' => $this->body,
            'image' => $this->image,
            'created_at' => $this->created_at,
        ];
    }
}
