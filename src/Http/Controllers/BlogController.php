<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.01.19
 * Time: 19:41
 */

namespace SashaAdm\Demoapi\Http\Controllers;



use Illuminate\Routing\Controller;
use SashaAdm\Demoapi\Blog;
use SashaAdm\Demoapi\Http\Resources\BlogResource;


class BlogController extends Controller
{

    public function index()
    {
        $blog = Blog::with('user')->get();
        return BlogResource::collection($blog);


    }

    public function show(Blog $blog)
    {
        return new BlogResource($blog);

    }

}