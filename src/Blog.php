<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28.01.19
 * Time: 19:25
 */

namespace SashaAdm\Demoapi;


use Illuminate\Database\Eloquent\Model;
use App\User;



class Blog extends Model
{

    protected $fillable = ['body'];

    public function user(){
        return $this->belongsTo(User::class);
    }
}