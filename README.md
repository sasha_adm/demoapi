composer require sashaadm/demoapi

php artisan vendor:publish --provider="SashaAdm\Demoapi\DemoapiServiceProvider" --tag=migrations
php artisan vendor:publish --provider="SashaAdm\Demoapi\DemoapiServiceProvider" --tag=img

php artisan migrate